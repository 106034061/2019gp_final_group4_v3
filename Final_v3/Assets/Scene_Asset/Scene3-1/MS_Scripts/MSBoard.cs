using UnityEngine;
using UnityEngine.UI;

public class MSBoard : MonoBehaviour {
  public GameObject mCellPrefab;

  public void Create(MSCell[,] mAllCells, int[] ans) {
    int n = MSGameManager.sideLen;

    for (int y = 0; y < n; ++y) {
      for (int x = 0; x < n; ++x) {
        GameObject newCell = Instantiate(mCellPrefab, transform);
        RectTransform rectTransform = newCell.GetComponent<RectTransform>();
        int sp = MSGameManager.sidePixels;
        rectTransform.anchoredPosition = new Vector2(x * sp + sp / 2, y * sp + sp / 2);
        mAllCells[x, y] = newCell.GetComponent<MSCell>();
        mAllCells[x, y].Setup(new Vector2Int(x, y), this);
      }
    }

    for (int _ = 0; _ < n * n / 8; ++_) { // this many bombs
      int x = Random.Range(0, n);
      int y = Random.Range(0, n);
      while (ans[x * n + y] == -1) {
        x = Random.Range(0, n);
        y = Random.Range(0, n);
      }
      ans[x * n + y] = -1;
    }

    int[] dx = { 0, 1, 0, -1, 1, 1, -1, -1 };
    int[] dy = { 1, 0, -1, 0, 1, -1, 1, -1 };
    for (int x = 0; x < n; ++x) {
      for (int y = 0; y < n; ++y) {
        if (ans[x * n + y] == -1) continue;
        int c = 0;
        for (int d = 0; d < 8; ++d) {
          int nx = x + dx[d];
          int ny = y + dy[d];
          if (nx < 0 || ny < 0 || nx == n || ny == n) continue;
          c += ans[nx * n + ny] == -1 ? 1 : 0;
        }
        ans[x * n + y] = c;
      }
    }
  }
}
