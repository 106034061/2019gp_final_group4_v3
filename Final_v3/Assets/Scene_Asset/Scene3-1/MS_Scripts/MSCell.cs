using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MSCell : MonoBehaviour, IPointerClickHandler {
  public Image mOutlineImage;
  public Vector2Int mBoardPosition = Vector2Int.zero;
  public MSBoard mBoard = null;
  public RectTransform mRectTransform = null;

  public void Setup(Vector2Int newBoardPosition, MSBoard newBoard) {
    mBoardPosition = newBoardPosition;
    mBoard = newBoard;
    mRectTransform = GetComponent<RectTransform>();
    mRectTransform.sizeDelta = new Vector2(MSGameManager.sidePixels, MSGameManager.sidePixels);
  }

  public void OnPointerClick(PointerEventData eventData) {
    if (eventData.button == PointerEventData.InputButton.Left) {
      MSGameManager.leftClickCell(gameObject);
    } else if (eventData.button == PointerEventData.InputButton.Middle) {
      Debug.Log("Middle click");
    } else if (eventData.button == PointerEventData.InputButton.Right) {
      MSGameManager.rightClickCell(gameObject);
    }
  }
}

