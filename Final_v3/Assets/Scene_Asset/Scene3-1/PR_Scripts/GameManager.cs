﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour {
  public Board mBoard;
  public static int sideLen = 21;
  public static int sidePixels = 40;

  void Start() { mBoard.Create(); }

  public void toggleShipOnCell() {
    GameObject clickedCell = EventSystem.current.currentSelectedGameObject;
    if (clickedCell.GetComponent<Cell>().mBoardPosition.x >= sideLen) return;
    if (clickedCell.GetComponent<Cell>().mBoardPosition.y >= sideLen) return;
    for (int i = 0; i < 2; ++i) {
      GameObject t = clickedCell.transform.GetChild(i).gameObject;
      t.SetActive(t.activeSelf ? false : true);
    }
  }
}
