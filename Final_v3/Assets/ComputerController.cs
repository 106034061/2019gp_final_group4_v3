﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ComputerController : MonoBehaviour {
  public GameObject Player;
  public const int Lights_UP_Distance = 5;
  const int Lights_Intensity_max = 20000;
  const int Lights_Intensity_min = 10000;
  private int intensity = 0;
  public GameObject Computer_Light;
  public AudioClip Computer_Audio;
  public AudioSource Computer_Audio_Source;
  public int Scene =3;
  void Start() {
    Player = GameObject.Find("PlayerController/first_person_controller");

  }

  // Update is called once per frame
  void Update() {
    if(Mathf.Abs(transform.position.x - Player.transform.position.x) < Lights_UP_Distance &&
        Mathf.Abs(transform.position.z - Player.transform.position.z) < Lights_UP_Distance ) {
      // Debug.Log("Light Trigger");
      intensity = Random.Range(Lights_Intensity_min, Lights_Intensity_max);
      Computer_Light.GetComponent<Light>().intensity = intensity;
      if(Input.GetKeyDown(KeyCode.Space) && Scene == 3) {
        Computer_Audio_Source.PlayOneShot(Computer_Audio);
        SceneManager.LoadScene("Scene3-1(foolfill)");

      }

    } else {
      Computer_Light.GetComponent<Light>().intensity = 0;
    }
  }
}
