﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovieController : MonoBehaviour
{

    public GameObject MovieCanvas;

    void Start()
    {
        MovieCanvas = GameObject.Find("MovieController/MovieCanvas");
        Invoke("Movie_End", 5.0f);
    }
    void Movie_End(){
        MovieCanvas.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
