﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;    // 記得加這行

public class TextController : MonoBehaviour
{
    public GameObject Text_Object;
    public string [] Narration_Text ;
    private int index = -1;

    void Start()
    {
        // Text_Object = GameObject.Find("TextController/TextCanvas/NarrationText");
        InvokeRepeating("TextUpdate", 5.0f, 5.0f);    
    }
    void TextUpdate(){

        if(index++ < Narration_Text.Length){
            // Text_Object.gameObject.GetComponent<Text> ().text = Narration_Text[index]; 
            GetComponent<Text> ().text = Narration_Text[index];
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
