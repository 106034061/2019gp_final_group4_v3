﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public GameObject Player;
    public string Status = "White";
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void OnTriggerEnter(Collider collision)
    {

        if (collision.gameObject.tag == "Player"){
            if( Status == "White"){
                Debug.Log("Level UP");
                SceneManager.LoadScene("scene1-3W");
            }
            else if (Status ==  "Black"){
                Debug.Log("Level UP");
                SceneManager.LoadScene("scene1-3B");
            }
        }
    }
}
