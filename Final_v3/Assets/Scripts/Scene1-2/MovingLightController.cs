﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingLightController : MonoBehaviour
{
    public GameObject Moving_Light;
    public GameObject Rotate_Light;
    // Start is called before the first frame update
    public float Moving_Speed = 3.0f;
    public int Rotate_Degree = 3;
    void Start()
    {
        InvokeRepeating("MoveUpdate", 0.0f, 3.0f);

    }

    // Update is called once per frame
    void Update()
    {
        Rotate_Light.transform.Rotate(0, Rotate_Degree, 0);
    }
    void MoveUpdate(){

    }
}
