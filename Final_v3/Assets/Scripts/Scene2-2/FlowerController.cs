﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlowerController : MonoBehaviour
{
    public GameObject Flower;
    public Vector3 New_Flower_Position;
    public GameObject Player;
    public GameObject Light;
    public GameObject Live0;
    public GameObject Killed;
    private int Light_Decreasing = 5; 
    private Vector3 Previous_Position;
    private Vector3 Current_Position;
    private Vector3 Move_Direction;
    private float Previous_Distance = 0;
    private float Current_Distance = 0;
    private float Delta_Distance = 0;
    private int State_Count = 0;
    private bool State_Update = false;
    private bool Light_Update = false;
    public int Die_Limit = 1;
    public GameObject Flower_Face;
    public GameObject StateController;
    public int Flower_Number=1;
    private bool Is_Killed;
    public ParticleSystem Flower_Particle;
    public GameObject Flower_Effect;

    void Start()
    {
        InvokeRepeating("MoveUpdate", 0.0f, 1.5f);
        // InvokeRepeating("StateUpdate", 0.0f, 1.5f);
        // InvokeRepeating("LightUpdate", 0.0f, 1.5f);
        Current_Position = Player.transform.position;
        Previous_Position = Player.transform.position;
    }
    void LightUpdate(){
        // if(Light_Update){
        Light.gameObject.GetComponent<Light>().intensity -= Light_Decreasing;
        // }
    }
    void StateUpdate(){
        State_Count++;
        Debug.Log("State_Counte++");
        if(State_Count == 1){
            Flower_Particle.Stop();
            Flower_Particle.Play();
            // New_Flower_Position = Flower.transform.position + Move_Direction;
            New_Flower_Position = Flower.transform.position + new Vector3(Move_Direction.x, 0 , Move_Direction.z);
            Flower.transform.position = New_Flower_Position;
            Flower_Effect.SetActive(true);
        }

    }
    void MoveUpdate(){
        Current_Position = Player.transform.position;
        Move_Direction = Current_Position - Previous_Position;
        Current_Distance = Vector3.Distance(Player.transform.position, Flower.transform.position);
        Delta_Distance = Current_Distance - Previous_Distance;
        if(Delta_Distance < -1.0f){
            Debug.Log("Near");
            // State_Update = true;
            StateUpdate();
            // Light_Update = true;
            LightUpdate();
        }
        else{
            Debug.Log("Far");
            // Light_Update = false;
            // State_Update = false; 
            
        }

        Previous_Distance = Current_Distance;
        Previous_Position = Current_Position;
    }
    void Update()
    {
        // Flower_Face.transform.LookAt(Player.transform);   
        // if(Mathf.Abs(transform.position.x - Player.transform.position.x) < 2 &&
        //     Mathf.Abs(transform.position.z - Player.transform.position.z) < 2){
        //     Debug.Log("Collide");
        //     Flower_Particle.Stop();
        //     Flower_Particle.Play();
        //     Live0.SetActive(false);
        //     Killed.SetActive(true);
        //     if(Flower_Number == 1){
        //         StateController.GetComponent<StateController>().Flower1_Die = true;
        //     }
        //     else if(Flower_Number == 2){
        //         StateController.GetComponent<StateController>().Flower2_Die = true;
        //     }
        //     else if (Flower_Number == 3){
        //         StateController.GetComponent<StateController>().Flower3_Die = true;
        //     }
        // }
    }
    void OnCollisionEnter(Collision collider)
    {
        if (collider.gameObject.CompareTag("Player")){
            Debug.Log("Collide");
            Flower_Particle.Stop();
            Flower_Particle.Play();
            Live0.SetActive(false);
            Killed.SetActive(true);
            if(Flower_Number == 1){
                StateController.GetComponent<StateController>().Flower1_Die = true;
            }
            else if(Flower_Number == 2){
                StateController.GetComponent<StateController>().Flower2_Die = true;
            }
            else if (Flower_Number == 3){
                StateController.GetComponent<StateController>().Flower3_Die = true;
            }
            
        }
    }

}
