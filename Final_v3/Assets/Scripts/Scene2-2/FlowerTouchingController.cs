﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerTouchingController : MonoBehaviour
{
    public int Flower_Number=1;
    private bool Is_Killed;
    public ParticleSystem Flower_Particle;
    public GameObject Live0;
    public GameObject Killed;
    public GameObject StateController;
    public GameObject Player;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Mathf.Abs(transform.position.x - Player.transform.position.x) < 2.5 &&
            Mathf.Abs(transform.position.z - Player.transform.position.z) < 2.5){
            Debug.Log("Collide");
            Flower_Particle.Stop();
            Flower_Particle.Play();
            Live0.SetActive(false);
            Killed.SetActive(true);
            if(Flower_Number == 1){
                StateController.GetComponent<StateController>().Flower1_Die = true;
            }
            else if(Flower_Number == 2){
                StateController.GetComponent<StateController>().Flower2_Die = true;
            }
            else if (Flower_Number == 3){
                StateController.GetComponent<StateController>().Flower3_Die = true;
            }
        }
    }
    void OnCollisionEnter(Collision collider)
    {
        if (collider.gameObject.CompareTag("Player")){
            Debug.Log("Collide");
            Flower_Particle.Stop();
            Flower_Particle.Play();
            Killed.SetActive(true);
            if(Flower_Number == 1){
                StateController.GetComponent<StateController>().Flower1_Die = true;
            }
            else if(Flower_Number == 2){
                StateController.GetComponent<StateController>().Flower2_Die = true;
            }
            else if (Flower_Number == 3){
                StateController.GetComponent<StateController>().Flower3_Die = true;
            }
            Live0.SetActive(false);
            
        }
    }
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player"){
            Debug.Log("Collide");
            Flower_Particle.Stop();
            Flower_Particle.Play();
            Live0.SetActive(false);
            Killed.SetActive(true);
            if(Flower_Number == 1){
                StateController.GetComponent<StateController>().Flower1_Die = true;
            }
            else if(Flower_Number == 2){
                StateController.GetComponent<StateController>().Flower2_Die = true;
            }
            else if (Flower_Number == 3){
                StateController.GetComponent<StateController>().Flower3_Die = true;
            }
            
        }
    }
}
