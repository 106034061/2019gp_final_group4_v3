﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
  public GameObject env;
  public Image score1450;
  public Sprite image1450;
  public GameObject myNameIsScore;

  bool isDragging;
  Vector3 init_position;

  private void Start() {
    init_position = transform.position;
    if (myNameIsScore) {
      myNameIsScore.name = "0";
      DontDestroyOnLoad(myNameIsScore);
    }
  }

  private void Update() {
    Vector3 dest = env.GetComponent<Scene31Env>().page3_dest.transform.position;
    if (isDragging && transform.position != dest) {
      transform.position = Input.mousePosition;
      if (gameObject.name == "A1") {
        if ((dest - transform.position).magnitude < 18f) {
          transform.position = dest;
          env.GetComponent<Scene31Env>().page3_solved <<= 1;
          env.GetComponent<Scene31Env>().page3_solved |= 1;
          if (gameObject.tag == "1450") {
            score1450.sprite = image1450;
            myNameIsScore.name = "-1450";
          } else if (gameObject.tag == "5") {
            myNameIsScore.name = "-0.5";
          }
        }
      }
    }
  }

  public void OnPointerDown(PointerEventData eventData) {
    RaycastHit2D hit = Physics2D.Raycast(eventData.position, Vector2.zero);
    isDragging = true;
  }

  public void OnPointerUp(PointerEventData eventData) {
    Vector3 dest = env.GetComponent<Scene31Env>().page3_dest.transform.position;
    if (gameObject.name != "A1" || transform.position != dest) transform.position = init_position;
    isDragging = false;
  }
}
