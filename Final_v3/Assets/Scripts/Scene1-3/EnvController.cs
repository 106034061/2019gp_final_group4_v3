﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvController : MonoBehaviour {
  public GameObject hole;
  public GameObject stone;
  private float timer;
  void Start() { timer = 0f; }
  void FixedUpdate() {
    if (timer < 8f && timer + Time.deltaTime >= 8f) hole.SetActive(true);
    timer += Time.deltaTime;
  }
}
