﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniPlayerController : MonoBehaviour {
  public float forwardSpeed = 7.0f;
  public float backwardSpeed = 2.0f;
  public float rotateSpeed = 2.0f;
  public float jumpPower = 4.0f;
  public float timeLeft = 10.0f;

  public GameObject camera;

  private float yaw = 0.0f;
  private float pitch = 0.0f;

  void Start() {

  }

  void Update() {
    float v = Input.GetAxis("Vertical");
    float h = Input.GetAxis("Horizontal");
    Vector3 velocity = Vector3.forward * v;

    gameObject.transform.Rotate(0, h * rotateSpeed, 0);
    transform.Translate(velocity * (0 < v ? forwardSpeed : backwardSpeed) * Time.fixedDeltaTime);

    transform.Rotate(transform.up, rotateSpeed * Input.GetAxis("Mouse X"));
    print(Input.mousePosition.y);
    // camera.transform.Rotate(camera.transform.right, camAng);
    // pitch -= rotateSpeed * Input.GetAxis("Mouse Y");
    // camera.transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
  }
}
