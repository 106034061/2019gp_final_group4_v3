﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wallpaper : MonoBehaviour {
  public GameObject stone;
  public GameObject wallpaper;

  private float timer = 0f;

  void Start() {

  }

  void Update() {

  }

  void FixedUpdate() {
    if (timer > 0f) {
      timer += Time.deltaTime;
      if (timer > 4f) {
        timer = -1f;
        wallpaper.SetActive(false);
      }
    }
  }

  void OnTriggerEnter(Collider other) {
    if (timer == 0f && other.gameObject.tag == "Player") {
      stone.SetActive(true);
      wallpaper.SetActive(true);
      timer = 0.001f;
    }
  }
}
