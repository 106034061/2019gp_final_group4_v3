using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Computer : MonoBehaviour {
  public GameObject cam;
  public List<GameObject> audios = new List<GameObject>();
  public static int cnt = 0;

  void Start() {
    cam = GameObject.Find("/PlayerController/first_person_controller/world_camera");
    for (int i = 1; i <= 4; ++i) audios.Add(GameObject.Find("/Track/audio" + i.ToString()));
  }

  void Update() {

  }

  void FixedUpdate() {

  }

  void OnTriggerEnter(Collider other) {
    if (other.gameObject.tag == "Player") {
      cam.GetComponent<CameraShake>().shakeDuration = 0.5f;
      audios[cnt % 4].GetComponent<AudioSource>().Play();
      cnt += 1;
    }
  }
}
