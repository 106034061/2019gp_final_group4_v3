﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GunController : MonoBehaviour{
    public GameObject Gun;
    public GameObject Pivot_Start;
    public GameObject Pivot_End;
    public GameObject Bullet;
    GameObject Bullet_Prefab;
    public AudioSource Gun_Audio_Source;
    public AudioClip Gun_Shot_Audio;
    private Vector3 Direction;
    public ParticleSystem Gun_Particles;
    public int Kill_Count = 0;
    void Start(){}
    void FixedUpdate(){
        GunShotUpdate();
        if(Kill_Count == 20 ){
            SceneManager.LoadScene("Scene_End");

        }
    }
    void GunShotUpdate(){
        Direction = Pivot_End.transform.position - Pivot_Start.transform.position;
        if (Input.GetKeyDown(KeyCode.K) || Input.GetMouseButtonDown(0)){
            Gun_Audio_Source.PlayOneShot(Gun_Shot_Audio);
            Bullet_Prefab = Instantiate(Bullet, Pivot_Start.transform.position, Quaternion.identity);
            Bullet_Prefab.GetComponent<Rigidbody>().velocity = Direction * 20;
            Gun_Particles.Stop();
            Gun_Particles.Play();
        }
    }
}
