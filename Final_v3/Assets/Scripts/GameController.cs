﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public string Level = "Scene1-1";
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L)){
            Debug.Log("Next Level");
            if(Level == "Scene0"){
                SceneManager.LoadScene("Scene0_to_1");
            }
            else if (Level == "Scene0_to_1"){
                SceneManager.LoadScene("Scene1-1");
            }
            else if( Level == "Scene1-1"){
                SceneManager.LoadScene("Scene1-2B");
            }
            else if(Level == "Scene1-2_B"){
                SceneManager.LoadScene("scene1-3B");
            }
            else if(Level == "Scene1-2_W"){
                SceneManager.LoadScene("scene1-3W");
            }
            else if(Level == "Scene1-3_B"){
                SceneManager.LoadScene("Scene2-2");
            }
            else if(Level == "Scene1-3_W"){
                SceneManager.LoadScene("Scene2-2");
            }
            else if(Level == "Scene2-2"){
                SceneManager.LoadScene("Scene3");
            }
            else if(Level == "Scene3"){
                SceneManager.LoadScene("Scene3-1(foolfill)");

            }
            else if(Level == "Scene3-1"){
                SceneManager.LoadScene("Scene3-1(foolfill)");

            }
            else if(Level == "Scene3-1(follfill)"){
                SceneManager.LoadScene("Scene3-2(minesweeper)");

            }
            else if(Level == "Scene3-2(minesweeper)"){
                SceneManager.LoadScene("Scene4");

            }
            else if(Level == "Scene3-3"){
                SceneManager.LoadScene("Scene4");
            }
            else if(Level == "Scene4"){
                SceneManager.LoadScene("Scene_End");

            }
            else if(Level == "Scene_End"){
                SceneManager.LoadScene("Scene0");

            }
        }
    
        else if (Input.GetKeyDown(KeyCode.K)){
            Debug.Log("Last Level");
            if(Level == "Scene0"){
                SceneManager.LoadScene("Scene0");
            }
            else if (Level == "Scene0_to_1"){
                SceneManager.LoadScene("Scene0");
            }
            else if( Level == "Scene1-1"){
                SceneManager.LoadScene("Scene0_to_1");
            }
            else if(Level == "Scene1-2_B"){
                SceneManager.LoadScene("Scene1-1");
            }
            else if(Level == "Scene1-2_W"){
                SceneManager.LoadScene("Scene1-1");
            }
            else if(Level == "Scene1-3_B"){
                SceneManager.LoadScene("Scene1-2B");
            }
            else if(Level == "Scene1-3_W"){
                SceneManager.LoadScene("Scene1-2W");
            }
            else if(Level == "Scene2-2"){
                SceneManager.LoadScene("scene1-3B");
            }
            else if(Level == "Scene3"){
                SceneManager.LoadScene("Scene2-2");
            }
            else if(Level == "Scene3-1"){
                SceneManager.LoadScene("Scene3");

            }
            else if(Level == "Scene3-2(minesweeper)"){
                SceneManager.LoadScene("Scene3-1(foolfill)");

            }
            else if(Level == "Scene3-3"){
                SceneManager.LoadScene("Scene3-2(minesweeper)");

            }
            else if(Level == "Scene4"){
                SceneManager.LoadScene("Scene3-3");

            }
            else if(Level == "Scene_End"){
                SceneManager.LoadScene("Scene4");
 
            }
        }
    }
}
