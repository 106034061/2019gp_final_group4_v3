﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleComponent : MonoBehaviour {

	void Start () {

        // 取得名為Player的GameObject
        GameObject player = GameObject.Find("player");

        // 由player這個GameObject上，取得型別為PlayerStatus的Component
        PlayerStatus status = player.GetComponent<PlayerStatus>();

        // 關閉位於Player上的PlayerStatus component物件
        status.enabled = false;
	}
}
