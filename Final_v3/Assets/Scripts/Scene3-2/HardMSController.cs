﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HardMSController : MonoBehaviour {
  public GameObject mCellPrefab;
  public static int n = 50;
  public static int m = 30;
  public float x0;
  public float y0;
  public float gap;
  public Sprite _sprite_flag;
  public Sprite _sprite_gray;
  public Sprite[] _sprite_bombs;
  public Sprite[] _sprite_nums;
  public static Sprite sprite_flag;
  public static Sprite sprite_gray;
  public static Sprite[] sprite_bombs;
  public static Sprite[] sprite_nums;

  public GameObject _d0, _d1, _d2, _d3, _d4, _d5;
  public static GameObject d0, d1, d2, d3, d4, d5;
  public Sprite[] _dnums;
  public static Sprite[] dnums;

  public Sprite[] _sprite_exps;
  public static Sprite[] sprite_exps;
  public Image _face;
  public static Image face;
  static float face_countdown;

  public static GameObject[,] cells;
  public static int[,] ans;
  public static int[,] cur;
  static int[] dx = { 0, 1, 0, -1, 1, 1, -1, -1 };
  static int[] dy = { 1, 0, -1, 0, 1, -1, 1, -1 };

  public static int num_flagged = 0;
  public static int num_bombs = 0;
  public static int score = 0;
  public static int opened_cnt = 0;

  public static float gameCountdown = 1000f;
  public static bool gameFrozen = false;
  public float lastCountdown = 3f;
  public int[] sudoku = { 5, 3, 4, 6, 7, 8, 9, 1, 2, 6, 7, 2, 1, 9, 5, 3, 4, 8, 1, 9, 8, 3, 4, 2, 5, 6, 7, 8, 5, 9, 7, 6, 1, 4, 2, 3, 4, 2, 6, 8, 5, 3, 7, 9, 1, 7, 1, 3, 9, 2, 4, 8, 5, 6, 9, 6, 1, 5, 3, 7, 2, 8, 4, 2, 8, 7, 4, 1, 9, 6, 3, 5, 3, 4, 5, 2, 8, 6, 1, 7, 9 };

  static void setBombNum(int v) {
    if (v < 0) v = 0;
    d0.GetComponent<Image>().sprite = dnums[v / 100];
    d1.GetComponent<Image>().sprite = dnums[v / 10 % 10];
    d2.GetComponent<Image>().sprite = dnums[v % 10];
  }

  static void setTimeNum(int v) {
    if (gameFrozen) return;
    if (v < 0) v = 0;
    if (v > 999) v = 999;
    d3.GetComponent<Image>().sprite = dnums[v / 100];
    d4.GetComponent<Image>().sprite = dnums[v / 10 % 10];
    d5.GetComponent<Image>().sprite = dnums[v % 10];
  }

  void Start() {
    d0 = _d0;
    d1 = _d1;
    d2 = _d2;
    d3 = _d3;
    d4 = _d4;
    d5 = _d5;
    dnums = _dnums;
    sprite_flag = _sprite_flag;
    sprite_gray = _sprite_gray;
    sprite_bombs = _sprite_bombs;
    sprite_nums = _sprite_nums;
    sprite_exps = _sprite_exps;
    face = _face;
    cells = new GameObject[n, m];
    ans = new int[n, m];
    cur = new int[n, m];
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        cells[i, j] = Instantiate(mCellPrefab, transform);
        cells[i, j].GetComponent<RectTransform>().anchoredPosition = new Vector2(x0 + i * gap, y0 + j * -gap);
        cells[i, j].name = (i * m + j).ToString();
        cur[i, j] = 0;
        ans[i, j] = 0;
      }
    }
    for (int i = 0; i < 9; ++i) for (int j = 0; j < 9; ++j) ans[32 + i, 7 + j] = sudoku[i * 9 + j];
    for (int _ = 0; _ < n * m / 4; ++_) { // this many bombs
      int x = Random.Range(0, n);
      int y = Random.Range(0, m);
      while (ans[x, y] != 0) {
        x = Random.Range(0, n);
        y = Random.Range(0, m);
      }
      ans[x, y] = -1;
      num_bombs += 1;
    }
    for (int x = 0; x < n; ++x) {
      for (int y = 0; y < m; ++y) {
        if (ans[x, y] != 0) continue;
        int c = 0;
        for (int d = 0; d < 8; ++d) {
          int nx = x + dx[d];
          int ny = y + dy[d];
          if (nx < 0 || ny < 0 || nx == n || ny == m) continue;
          c += ans[nx, ny] == -1 ? 1 : 0;
        }
        ans[x, y] = c;
      }
    }
  }

  void Update() {
    if (gameCountdown < 1000f) gameCountdown -= Time.deltaTime;
    int c = (int) gameCountdown;
    setTimeNum(c);
    setBombNum(num_bombs - num_flagged);
    if (face_countdown > 0f) {
      face_countdown -= Time.deltaTime;
      if (face_countdown <= 0f) {
        if (face.sprite == sprite_exps[1]) {
          face.sprite = sprite_exps[0];
        }
      }
    } else if (gameFrozen) {
      if (lastCountdown == 3f) { // first time?
        for (int i = 0; i < n; ++i) {
          for (int j = 0; j < m; ++j) {
            if (cur[i, j] == 2) continue;
            int truth = ans[i, j];
            if (cur[i, j] == 0) cells[i, j].GetComponent<Image>().sprite = truth == -1 ? sprite_bombs[2] : sprite_nums[truth];
            else cells[i, j].GetComponent<Image>().sprite = truth == -1 ? sprite_bombs[2] : sprite_bombs[1];
          }
        }
      }
      if (lastCountdown > 0f) {
        lastCountdown -= Time.deltaTime;
        if (lastCountdown <= 0f) {
          SceneManager.LoadScene("Scene4");
        }
      }
    }
  }

  public static void leftClickCell(GameObject c) {
    if (gameFrozen) return;
    gameCountdown -= 0.001f;
    int px = int.Parse(c.name) / m;
    int py = int.Parse(c.name) % m;
    if (cur[px, py] != 0) return; 

    face_countdown = 0.6f;
    face.sprite = sprite_exps[1];

    Queue<int> que = new Queue<int>();
    que.Enqueue(px * m + py);
    cur[px, py] = 2;
    while (0 < que.Count) {
      int x = que.Peek() / m;
      int y = que.Peek() % m;
      que.Dequeue();
      int truth = ans[x, y];
      cells[x, y].GetComponent<Image>().sprite = truth == -1 ? sprite_bombs[0] : sprite_nums[truth];
      if (truth == -1) {
        face.sprite = sprite_exps[3];
        gameFrozen = true;
      } else {
        opened_cnt += 1;
        if (opened_cnt + num_bombs == n * m) {
          face.sprite = sprite_exps[2];
          gameFrozen = true;
        }
      }
      if (ans[x, y] != 0) continue;
      for (int d = 0; d < 4; ++d) {
        int nx = x + dx[d];
        int ny = y + dy[d];
        if (nx < 0 || ny < 0 || nx == n || ny == m) continue;
        if (cur[nx, ny] != 2) {
          que.Enqueue(nx * m + ny);
          cur[nx, ny] = 2;
        }
      }
    }
  }

  public static void rightClickCell(GameObject c) {
    if (gameFrozen) return;
    gameCountdown -= 0.001f;
    int px = int.Parse(c.name) / m;
    int py = int.Parse(c.name) % m;
    if (cur[px, py] == 2) return; // clicked
    cur[px, py] ^= 1;
    num_flagged += cur[px, py] == 1 ? +1 : -1;
    c.GetComponent<Image>().sprite = cur[px, py] == 1 ? sprite_flag : sprite_gray;
  }
}
